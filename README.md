# 20230323-SindhuThota-NYCSchools

NYCSchools app shows the list of new york city schools

## Requirements
You will need to have the following tools in place before working on this application:

1. [Xcode](./.xcode-version)

## Setting it up

1. Clone the repository
  - `$ git clone https://gitlab.com/jpmc7824012/20230323-SindhuThota-NYCSchools.git`
  - `$ cd 20230310-SindhuThota-Chase/NYCSchools/`
  - `$ open NYCSchools.xcodeproj` 

## How the app works 

Use Case 1: Upon launching the app, the user will see the launching screen. If the app has internet access, the app will display the list of new york city school. If the app doesn't have internet access, app displays the fallbackview with message.
app launching

<img src="Screenshots/Usecase1.0.png"  width="393" height="852">

If app has internet access

<img src="Screenshots/Usecase1.3.png"  width="393" height="852">

<img src="Screenshots/Usecase2.png"  width="393" height="852">

If app has no internet access.

<img src="Screenshots/Usecase1.1.png"  width="393" height="852">

<img src="Screenshots/Usecase1.2png.png"  width="393" height="852">



Use Case 2: Upon selecting one of the school, the app displays the SAT scores of that selected school along with the location of the school.


<img src="Screenshots/Usecase3.png"  width="393" height="852">

Use Case 3: if user selects the school which doesn't have SATscores displays only the location of the school. 


<img src="Screenshots/Usecase3.1.png"  width="393" height="852">


## Architecture( Design Pattern) 

To build this application, I used MVVM Design pattern. 

## Tests 
Run the tests using Xcode
    - Open NYCSchools.xcodeproj 
    - Select an iPhone 14 Pro simulator and build and run the project (Menu Product > Run or `⌘ + R`)
    - Run the unit tests (Menu Product > Test or `⌘ + U`)

  Unit tests will be executed as part of the test step


![image info](Screenshots/TestCases.png) 


Thank you 

Sindhu Thota 

